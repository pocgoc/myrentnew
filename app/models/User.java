package models;

import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;

import play.db.jpa.Model;
@Entity
public class User extends Model
{
	public String firstName;
	public String lastName;
	public String email;
	public String password;
	public String telePhone;
	public String userStatus;
	
	public User(String firstName, String lastName, String email, String password, String telePhone, String userStatus)
	{
		this.firstName = firstName;
		this.lastName  = lastName;
		this.email     = email;
		this.password  = password;
		this.telePhone = telePhone;
		this.userStatus = userStatus;
	}
	
	public static User findByEmail(String email)
	{
		return find("email", email).first();
	}

	public boolean checkPassword(String password)
	{
		return this.password.equals(password);
	}
}
