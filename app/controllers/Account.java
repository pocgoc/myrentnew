package controllers;

import play.*;
import play.mvc.*;

import java.util.*;

import models.*;

public class Account extends Controller
{
  public static void signup()
  {
    render();
  }

  public static void login()
  {
    render();
  }

  public static void logout()
  {
    session.clear();
    index();
  }

  public static void index()
  {
    render();
  }
  
  public static User currentlyLoggedIn()
  {
    User user = null;
    if (session.contains("logged_in_userid"))
    {
      String userId = session.get("logged_in_userid");
      user = User.findById(Long.parseLong(userId));
    }
    else
    {
      index();
    }
    return user;
  }
  
  public static void register(String firstName, String lastName, String email, String telePhone, String password, String userStatus)
  {
    Logger.info(firstName + " " + lastName + " " + email + " " + password + " " + telePhone + " " + userStatus);
    
    User user = new User(firstName, lastName, email, telePhone, password, userStatus);
    user.save();
    Welcome.index();
  }

  
  public static void authenticate(String email, String password)
  {
    Logger.info("Attempting to authenticate with " + email + ":" +  password);

    User user = User.findByEmail(email);
    if ((user != null) && (user.checkPassword(password) == true))
    {
      Logger.info("Authentication successful");
      session.put("logged_in_userid", user.id);
      Welcome.index();

    }
    else
    {
      Logger.info("Authentication failed");
      login();  
    }
  }
}